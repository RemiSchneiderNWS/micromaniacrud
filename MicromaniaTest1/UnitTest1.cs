using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicromaniaCRUD.Controllers;
using MicromaniaCRUD;
using MicromaniaCRUD.Model;
using System.Linq;

namespace MicromaniaTest1
{   
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void getTest()
        {
            MicromaniaController test = new MicromaniaController();
            Assert.IsNotNull(test.Gets());
        }
        [TestMethod]
        public void postTest()
        {
            MicromaniaController test = new MicromaniaController();
            int tailleI = test.ListDesJeux.Count();
            Jeu game = new Jeu {Id= 55, Name ="Fallout", categorie = "rpg", DateDeSortie="23/04/2015" };
            test.Save(game);
            int tailleF = test.ListDesJeux.Count();
            Assert.AreNotEqual(tailleF, tailleI);
            
        }

        [TestMethod]

        public void deleteTest()
        {
            MicromaniaController test = new MicromaniaController();
            int tailleI = test.ListDesJeux.Count();
            test.Delete(2);
            int tailleF = test.ListDesJeux.Count();
            Assert.AreNotEqual(tailleF, tailleI);
        }

        [TestMethod]

        public void PutTest()
        {
            MicromaniaController test = new MicromaniaController();
            
            Jeu game1 = new Jeu { Id = 55, Name = "Fallout", categorie = "rpg", DateDeSortie = "23/04/2015" };
            var game2 = test.ListDesJeux[2];

            test.change(game1,2);
            var game3 = test.ListDesJeux[2];
            Assert.AreNotEqual(game2, game3);

        }

    }
}
