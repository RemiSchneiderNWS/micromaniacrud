using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicromaniaCRUD.Model;

namespace MicromaniaCRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MicromaniaController : Controller
    {
        public List<Jeu> ListDesJeux = new List<Jeu>()
         {
             new Jeu(){Id = 1, Name = "GTA 5", categorie = "open-world", DateDeSortie ="17/09/13"  },
             new Jeu(){Id = 2, Name = "Diablo 3", categorie = "hack'n slash", DateDeSortie ="15/05/2012"  },
             new Jeu(){Id = 3, Name = "Vindictus", categorie = "mmo", DateDeSortie ="21/01/2010"  }
         };

        // GET: MicromaniaController/get
        [HttpGet]
        public ActionResult Gets()
        {
            if (ListDesJeux.Count != 0)
            {
                return Ok(ListDesJeux);
            }else
            {
                return NotFound("No list found");
            }
        }

        // POST:  MicromaniaController/Create
        [HttpPost]

        public ActionResult Save(Jeu jeu)
        {
            ListDesJeux.Add(jeu);
            if(ListDesJeux.Count == 0)
            {
                return NotFound("No list found");
            }
            return Ok(ListDesJeux);
               
            
        }

             
        // DELETE: MicromaniaController/Delete
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var jeu = ListDesJeux.SingleOrDefault(x => x.Id == id);
            if (jeu == null)
            {
                return NotFound("No game found");
            }

            ListDesJeux.Remove(jeu);

            if (ListDesJeux.Count == 0)
            {
                return NotFound("No list found");
            }
            return Ok(ListDesJeux);
        }

        // PUT : MicromaniaController/change
        [HttpPut]
        public ActionResult change(Jeu game,int id)
        {
            //fonctionne pas
            /*var jeu = ListDesJeux.SingleOrDefault(x => x.Id == id);
            ListDesJeux.RemoveAll(i => jeu.Id == id);
            ListDesJeux.Insert(id, game);*/

            ListDesJeux[id] = game;

            if (ListDesJeux.Count == 0)
            {
                return NotFound("No list found");
            }
            return Ok(ListDesJeux);

        }
    }
}
